from qiskit import QuantumProgram
from qiskit import Qconfig
import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg as la
from qiskit.tools.visualization import plot_histogram, plot_state
import os
import shutil
from qiskit.tools.visualization import latex_drawer
import pdf2image
def circuitImage(circuit, basis):
    """Obtain the circuit in image format
    Note: Requires pdflatex installed (to compile Latex)
    Note: Required pdf2image Python package (to display pdf as image)
    """
    filename='circuit'
    tmpdir='tmp/'
    if not os.path.exists(tmpdir):
        os.makedirs(tmpdir)
    latex_drawer(circuit, tmpdir+filename+".tex", basis=basis)
    os.system("/usr/local/texlive/2017/bin/x86_64-linux/pdflatex -output-directory {} {}".format(tmpdir, filename+".tex"))
    images = pdf2image.convert_from_path(tmpdir+filename+".pdf")
    images[0].show(); 
    shutil.rmtree(tmpdir)
    return;
qp = QuantumProgram()
qp.set_api(Qconfig.APItoken, Qconfig.config["url"]) 
def chunksToString(key):
    skey="";
    for i in range(len(key)):
        skey=skey+key[i];
    return skey;
def generateRandomBits(numOfQubits,backend,numOfBits):
    q=qp.create_quantum_register('q',numOfQubits);
    r=qp.create_classical_register('r',numOfQubits);
    hgate=qp.create_circuit('HGate',[q],[r]);
    for i in range(numOfQubits):
        hgate.h(q[i]);
        hgate.measure(q[i],r[i]);
    results=qp.execute('HGate',backend,shots=1);
    key=[];
    if numOfBits < 24:
        key.append(list(results.get_counts('HGate').keys())[0][:numOfBits]);
    else:
        key.append(list(results.get_counts('HGate').keys())[0]);
    numOfBits=numOfBits-numOfQubits;
    while numOfBits>0:
        for i in range(numOfQubits):
            hgate.h(q[i]);
            hgate.measure(q[i],r[i]);
        results=qp.execute('HGate',backend,shots=1);
        if numOfBits < 24:
            key.append(list(results.get_counts('HGate').keys())[0][:numOfBits]);
        else:
            key.append(list(results.get_counts('HGate').keys())[0]);
        numOfBits=numOfBits-numOfQubits;
    return key;
def makeQuantumInfo(numOfQubits,bitString,nameOfCircuit,nameOfQRegister,nameOfCRegister):
    #they must be on 0
    qc=qp.get_circuit(nameOfCircuit);
    qr=qp.get_quantum_register(nameOfQRegister);
    cr=qp.get_classical_register(nameOfCRegister);
    for i in range(len(bitString)):
        if bitString[i]=='1':
            qc.x(qr[i]);
        else:
            qc.iden(qr[i]);
    return;
def checkCorrectness(aliceKey,bobKey,aliceBases,bobBases):
    numOfBits=len(aliceKey);
    for i in range(numOfBits):
        if aliceBases[i]==bobBases[i]:
            if aliceKey[i]!=bobKey[i]:
                return 0;
    return 1;
def BB84FirstPart(backend,numOfBits):
    details=qp.get_backend_configuration(backend);
    numOfQubits=details['n_qubits'];
    q=qp.create_quantum_register('qBB84',numOfQubits);
    r=qp.create_classical_register('rBB84',numOfQubits);
    BB84FirstPart=qp.create_circuit('BB84FirstPart',[q],[r]);
    aliceKey=generateRandomBits(numOfQubits,backend,numOfBits);
    aliceBases=generateRandomBits(numOfQubits,backend,numOfBits);
    bobBases=generateRandomBits(numOfQubits,backend,numOfBits);
    numOfChunks=len(aliceKey);
    wireLength=5;
    bobKey=[];
    for i in range(numOfChunks):
        BB84FirstPart=qp.create_circuit('BB84FirstPart',[q],[r]);
        print('Alice1',aliceKey[i]);
        print('Alice2',aliceBases[i]);
        print('Bob',bobBases[i]);
#        for j in range(len(aliceBases[i])):
#            BB84FirstPart.measure(q[j],r[j]);
        
        #verificare
#        print(qp.get_qasm('BB84FirstPart'));
#        results=qp.execute('BB84FirstPart',backend,shots=1);
#        print(results.get_counts('BB84FirstPart'));
        
        makeQuantumInfo(numOfQubits,aliceKey[i],'BB84FirstPart','qBB84','rBB84');
        
        #aici Alice pregateste qubitii
        for j in range(len(aliceBases[i])):
            if aliceBases[i][j]=='1':
                BB84FirstPart.h(q[j]);
            else:
                BB84FirstPart.iden(q[j]);
        
        #partea de transmite adica identitati
        for j in range(wireLength):
            BB84FirstPart.iden(q[j]);
            
        #aici Bob masoara

        for j in range(len(bobBases[i])):
            if bobBases[i][j]=='1':
                BB84FirstPart.h(q[j]);
            else:
                BB84FirstPart.iden(q[j]);
            BB84FirstPart.measure(q[j],r[j]);
        #print(qp.get_qasm('BB84FirstPart'));    
        results=qp.execute('BB84FirstPart',backend,shots=1);
        print(results.get_counts('BB84FirstPart'));
        measuredKey=list(results.get_counts('BB84FirstPart').keys())[0][::-1];
        measuredKey=measuredKey[0:len(aliceKey[i])];
        print(measuredKey);
        print('Result',checkCorrectness(aliceKey[i],measuredKey,aliceBases[i],bobBases[i]));
        bobKey.append(measuredKey);
        qp.destroy_circuit('BB84FirstPart');
    return bobBases,bobKey,aliceBases,aliceKey;
def getSecretKey(aliceKey,aliceBases,bobKey,bobBases):
    key='';
    for i in range(len(aliceKey)):
        for j in range(len(aliceKey[i])):
            if aliceBases[i][j]==bobBases[i][j]:
                if aliceKey[i][j]!=bobKey[i][j]:
                    return -1;
                key=key+aliceKey[i][j];
    return key;
backend='ibmqx_qasm_simulator';
numOfBits=64;
bobBases,bobKey,aliceBases,aliceKey=BB84FirstPart(backend,numOfBits);
print('Bob Bases',bobBases);
print('Bob Key',bobKey);
print('Alice Base',aliceBases);
print('Alice Key',aliceKey);
key=getSecretKey(aliceKey,aliceBases,bobKey,bobBases);
print('Shared key is:',key);
print('The procent of saved key is:',(len(key)/numOfBits)*100);
